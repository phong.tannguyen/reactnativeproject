import 'react-native-gesture-handler';
import * as React from 'react';
import { Button, View, TouchableOpacity, Text } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import HomeScreen from './Home';

const Profile = ({navigation}) => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Profile Screen</Text>
      <Button title="Home Page" onPress={() => navigation.navigate('Home')} />
    </View>
  );
};

export default Profile;