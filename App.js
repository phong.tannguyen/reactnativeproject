import 'react-native-gesture-handler';
import * as React from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import UserListScreen from './pages/UserList';
import LoginScreen from './pages/Login';
import DetailsScreen from './pages/Details';
import SignInScreen from './pages/SignIn';
import Custom_Side_Menu from './pages/menu/LeftDrawer';

const App = createSwitchNavigator({
  Login: {
    screen: LoginScreen,
  },
  UserList: {
    screen: UserListScreen,
  },
  Details: {
    screen: DetailsScreen,
  },
  SignIn: {
    screen: SignInScreen,
  },
}
// ,{
//   contentComponent: Custom_Side_Menu,
//   drawerWidth: Dimensions.get('window').width - 130,
// }
);

export default createAppContainer(App);